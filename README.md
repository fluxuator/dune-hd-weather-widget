# Dune HD Weather widget

> This project is deprecated and not maintained anymore

### Compile installer

```bash

$ makeself/makeself.sh src/installer/ tmp/install.sh "Dune HD Weather widget installer" ./install.sh && \
  gzip tmp/install.sh && \
  dsf_pack -e tmp/install.sh.gz -o tmp/dune_service_fix_widget_0.7_install.dsf && \
  rm tmp/install.sh.gz

```

### Compile uninstaller

```bash

$ gzip -c src/uninstaller/uninstall.sh > tmp/uninstall.sh.gz && \
  dsf_pack -e tmp/uninstall.sh.gz -o tmp/dune_service_fix_widget_uninstall.dsf && \
  rm tmp/uninstall.sh.gz

```