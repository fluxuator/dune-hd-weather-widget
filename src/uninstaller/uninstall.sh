#!/bin/sh

umount -l /firmware/scripts/run_widget.sh

[ -f "/persistfs/scripts/run_widget.sh" ] && rm -f /persistfs/scripts/run_widget.sh
[ -f "/flashdata/scripts/run_widget.sh" ] && rm -f /flashdata/scripts/run_widget.sh
[ -f "/config/scripts/run_widget.sh" ] && rm -f /config/scripts/run_widget.sh

rm -f /tmp/scripts/run_widget.sh
rm -f /config/boot/fix_widget.sh
rm -f /tmp/weather.txt