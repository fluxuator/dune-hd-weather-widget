#!/bin/sh

exec >/tmp/run/fix_widget.log 2>&1

date
echo
echo "Widget by Fluxuator will be copied from "

if mount | grep -q " /persistfs "; then
    SCRIPT_DIR="/persistfs"
elif mount | grep -q " /flashdata "; then
    SCRIPT_DIR="/flashdata"
else
    SCRIPT_DIR="/config"
fi

SCRIPT_DIR="$SCRIPT_DIR/scripts"

echo $SCRIPT_DIR
echo

TMP_SCRIPT_DIR="/tmp/scripts"
WEATHER_FILE="/tmp/weather.txt"

echo "0 init Инициализация..." >"$WEATHER_FILE.tmp"

mv -f "$WEATHER_FILE.tmp" "$WEATHER_FILE"

[ ! -d "$TMP_SCRIPT_DIR" ] && mkdir -p "$TMP_SCRIPT_DIR"

echo "to"
echo "$TMP_SCRIPT_DIR"

cp -a "$SCRIPT_DIR/run_widget.sh" "$TMP_SCRIPT_DIR/run_widget.sh"

mount --bind "$TMP_SCRIPT_DIR/run_widget.sh" /firmware/scripts/run_widget.sh

sync

if mount | grep -q "run_widget.sh"; then
    echo "Fixed script run_widget.sh is mounted successfully"
else
    echo "Mounting of run_widget.sh was failed"
fi

crontab -u root -l | grep -v 'run_widget.sh' | crontab -u root -

(crontab -u root -l ; echo "10 * * * * /firmware/scripts/run_widget.sh") | crontab -u root -

/firmware/scripts/run_widget.sh

echo "Done"