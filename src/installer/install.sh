#!/bin/sh

if mount | grep -q " /persistfs "; then
    SCRIPT_DIR="/persistfs"
elif mount | grep -q " /flashdata "; then
    SCRIPT_DIR="/flashdata"
else
    SCRIPT_DIR="/config"
fi

SCRIPT_DIR="$SCRIPT_DIR/scripts"

[ ! -d "$SCRIPT_DIR" ] && mkdir -p "$SCRIPT_DIR"

mv ./run_widget.sh "$SCRIPT_DIR/run_widget.sh"

[ ! -d "/config/boot" ] && mkdir -p "/config/boot"

mv ./fix_widget.sh /config/boot/fix_widget.sh

/config/boot/fix_widget.sh

exit 0