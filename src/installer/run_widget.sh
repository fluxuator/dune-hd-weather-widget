#!/bin/sh

###########################################################################

LOCK_FILE="/tmp/run/widget.lock"
LOG_FILE="/tmp/run/widget.log"

GET_LOCATION_SCRIPT_FILE="/firmware/scripts/run_get_location_info.sh"
LOCATION_INFO_FILE="/tmp/location_info.properties"
PERSISTENT_LOCATION_INFO_FILE="/config/location_info.properties"

# Weather file format:
#   temp type city
# E.g.:
#   +10C clear London, UK

# Location info file format:
#   city:country

update_weather_info() {
    res_temp=$1
    res_type=$2
    p_location_caption=$3

    if [ "$p_location_caption" = "" ]; then
        res_temp="0"
        p_location_caption="Укажите город"
    fi

    WEATHER_FILE="/tmp/weather.txt"
    echo "$res_temp $res_type $p_location_caption" >"$WEATHER_FILE.tmp"
    mv -f "$WEATHER_FILE.tmp" "$WEATHER_FILE"

    echo "Resulting weather info:"
    echo "-----"
    cat "$WEATHER_FILE"
    echo "-----"
    date
}

weather_cond_mapping(){
    # Convert openweathermap icon code to DUNE format
    case "$1" in
        01)
            echo 'clear';;
        02)
            echo 'clear_clouds';;
        03)
            echo 'clouds';;
        04)
            echo 'heavy_clouds';;
        09)
            echo 'rain';;
        10)
            echo 'heavy_rain';;
        11)
            echo 'thunderstorm';;
#       *)
#           echo 'heavy_thunderstorm';;
#       13)
#           echo 'snow';;
        13)
            echo 'heavy_snow';;
        *)
            echo 'unknown';;
    esac
}

###########################################################################

(set -C; echo $$ >"$LOCK_FILE") 2>/dev/null || exit
trap "rm -f $LOCK_FILE" 0 2 15

###########################################################################

# NOTE: it is essential to invoke run_get_location_info after checking lock
# file.
if [ -f "$GET_LOCATION_SCRIPT_FILE" ];
    then "$GET_LOCATION_SCRIPT_FILE"
fi

###########################################################################

widget_enabled="yes"
widget_country=""
widget_city=""

if [ -f /config/settings.properties ]; then
    exec <"/config/settings.properties"
    while true; do
        read key op value
        [ "$key" = "" ] && break
        [ "$key" = "widget_enabled" ] && widget_enabled="$value"
        [ "$key" = "widget_country" ] && widget_country="$value"
        [ "$key" = "widget_city" ] && widget_city="$value"
    done
fi

[ "$widget_enabled" = "yes" ] || exit

[ -f "/tmp/run/disable_run_widget" ] && exit

exec >"$LOG_FILE" 2>&1
date

###########################################################################

echo "City (settings): $widget_city"
echo "Country (settings): $widget_country"

p_city="$widget_city"
p_country="$widget_country"

if [ "$p_city" = "" -o "$p_country" = "" ]; then
    p_location_info_file=""
    if [ -f "$LOCATION_INFO_FILE" ]; then
        p_location_info_file="$LOCATION_INFO_FILE"
    elif [ -f "$PERSISTENT_LOCATION_INFO_FILE" ]; then
        p_location_info_file="$PERSISTENT_LOCATION_INFO_FILE"
    fi

    if [ "$p_location_info_file" != "" ]; then
        loaded_city=""
        loaded_country=""

        echo "Loading location info from ${p_location_info_file}..."

        exec <"$p_location_info_file"
        while true; do
            read key op value
            [ "$key" = "" ] && break
            [ "$key" = "city" ] && loaded_city="$value"
            [ "$key" = "country" ] && loaded_country="$value"
        done

        echo "City (loaded): $loaded_city"
        echo "Country (loaded): $loaded_country"

        [ "$p_city" = "" ] && p_city="$loaded_city"
        [ "$p_country" = "" ] && p_country="$loaded_country"
    fi
fi

if [ "$p_city" = "" ]; then
    echo "Failed to get location info"
    update_weather_info 0 '-' 'не определен (1)';
    exit
fi

echo "City: $p_city"
echo "Country: $p_country"

###########################################################################

echo "Retrieving weather info..."

if [ "$p_city" == "" ]; then
    p_location="$p_country"
    p_location_caption="$p_country"
elif [ "$p_country" == "" ]; then
    p_location="$p_city"
    p_location_caption="$p_city"
else
    p_location="$p_city,$p_country"
    p_location_caption="$p_city"
fi
echo "Location: $p_location"
echo "Location caption: $p_location_caption"

p_location=`echo "$p_location" | sed 's/ /%20/g'`

# Get weather info (API 2.1)
echo "Fetch current weather requesting by URL: $url"
url="http://openweathermap.org/data/2.1/find/name?q=$p_location"
tmp="/tmp/openweather_city.tmp"
cond=""
temp=""
units="C"
if wget -O "$tmp" "$url"; then
    [ "$units" = F ] && temp_field_name="temp_f" || temp_field_name="temp_c"
    temp=`sed -e 's/^.*"temp":\([^",]*\).*$/\1/' $tmp  | awk '{ printf("%.1f", $1) }'`
    cond=`sed 's/^.*"weather".*"icon":"\([0-9]*\)[dn]".*$/\1/' $tmp`

    [ "$widget_city" = "" ] && p_location_caption=`sed 's/^.*"name":"\([a-z]*\)".*$/\1/I' $tmp`
fi

# Round up the temperature by bash dirty hack
intPart=`echo $temp | cut -d '.' -f1`
restPart=`echo $temp | cut -d '.' -f2`
restPart=`echo $restPart | cut -c1`
if [ "$restPart" -gt 4 ]; then
    (( intPart++ ))
fi

if [ "$intPart" -lt 203 ]; then
    intPart=273
fi

temp=`expr $intPart - 273`

cat "$tmp"
rm -f "$tmp"
echo -e "\n -----"
echo "Condition: $cond"
echo "Temperature: $temp$units"
echo " -----"

if [ "$cond" = "" -o "$temp" = "" ]; then
    echo "Failed to retrieve weather info"
    exit
fi

echo "cond = '$cond'" >> "$LOG_FILE"
echo "temp = '$temp'" >> "$LOG_FILE"

res_type=$(weather_cond_mapping $cond)
res_temp="$temp$units"
[ "$temp" -ge 0 ] && res_temp="+$res_temp"

update_weather_info $res_temp $res_type $p_location_caption;

###########################################################################